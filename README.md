
SLEPc: Scalable Library for Eigenvalue Problem Computations
===========================================================

**WARNING**: This is no longer the official repository of the SLEPc project.

The new repository is hosted at [gitlab.com](http://gitlab.com/slepc/slepc).
This clone is kept at bitbucket.org for backward compatibility of PETSc's `--download-slepc` option, and it contains commits only up to Dec 10, 2019.

To update your local repository to use the new location, you need to do

    $ git remote set-url origin git@gitlab.com:slepc/slepc.git 

or

    $ git remote set-url origin https://gitlab.com/slepc/slepc.git

